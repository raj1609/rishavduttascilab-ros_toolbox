cmake_minimum_required(VERSION 2.8.3)
project(autonomous_quad)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  geometry_msgs
  sensor_msgs

)


catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS roscpp rospy std_msgs 

)


include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include ${catkin_INCLUDE_DIRS}
)
add_executable(gps src/gps.cpp)
target_link_libraries(gps ${catkin_LIBRARIES})

add_executable(heading_director src/heading_director.cpp)
target_link_libraries(heading_director ${catkin_LIBRARIES})

add_executable(imu src/imu.cpp)
target_link_libraries(imu ${catkin_LIBRARIES})

add_executable(obstacle_avoider src/obstacle_avoider.cpp)
target_link_libraries(obstacle_avoider ${catkin_LIBRARIES})

add_executable(obstacle_detector src/obstacle_detector.cpp)
target_link_libraries(obstacle_detector ${catkin_LIBRARIES})

add_executable(path_planner src/path_planner.cpp)
target_link_libraries(path_planner ${catkin_LIBRARIES}




#############
## Install ##
#############





)