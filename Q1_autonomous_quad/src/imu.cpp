#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/Imu.h"
#include <ros/subscriber.h>
#include <ros/publisher.h>
#include <ros/node_handle.h>
#include <cmath>

using namespace std;

float yaw;

std_msgs::Float64 imu1;

void chatterCallback(const sensor_msgs::Imu& msg)
{

  float q0 = msg.orientation.x;
  float q1 = msg.orientation.y;
  float q2 = msg.orientation.z;
  float q3 = msg.orientation.w;

  // Source wikipedia - Quaternion
  yaw = atan2(2*(q0*q3 + q2*q2), 1 - 2*(q3*q3 + q2*q2));
  yaw = yaw*180/3.14;
  //cout << "Angle = " << yaw << endl;

  imu1.data = yaw;
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talkeri");
  ros::NodeHandle n,n1;
  ros::Subscriber sub = n.subscribe("ardrone/imu", 1, chatterCallback);
  ros::Publisher imu_pub = n1.advertise<std_msgs::Float64>("yaw_quad", 1);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {

    imu_pub.publish(imu1);

    ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}