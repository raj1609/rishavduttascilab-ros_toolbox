#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Int64MultiArray.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/Point.h>
#include <gps_common/conversions.h>
#include <sensor_msgs/NavSatStatus.h>
#include <ros/subscriber.h>
#include <ros/publisher.h>
#include <ros/node_handle.h>
#include <cmath>

using namespace std;

//float roll;
double destX,destY,latitude,longitude;
std_msgs::Int64MultiArray theta_dist;

geometry_msgs::Point global;
void chatterCallback(const geometry_msgs::Point& msg)
{
  double currentX, currentY;
  currentX= msg.x;
  currentY= msg.y;
  theta_dist.data[0]=(( atan2(destY-currentY,destX-currentX))*180/3.14);
  theta_dist.data[0]=(sqrt( (destY-currentY)*(destY-currentY) + (destX-currentX)*(destX-currentX)));
  

}


int main(int argc, char **argv)
{
  
  char zone;
  cout<< "enter latitude:";
  cin >> latitude;
  cout<< "enter longitude:";
  cin >> longitude;
  gps_common::LLtoUTM(latitude,longitude,destY,destX, &zone);
  global.x = destX;
  global.y = destY;
  ros::init(argc, argv, "talkerhd");
  ros::NodeHandle n,n1;
  ros::Subscriber sub = n.subscribe("global_xy", 1, chatterCallback);
  ros::Publisher theta_dist_pub = n1.advertise<std_msgs::Int64MultiArray>("theta_r_f", 1);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {

    theta_dist_pub.publish(theta_dist);

    ros::spinOnce();

    loop_rate.sleep();
   
  }



  return 0;
}