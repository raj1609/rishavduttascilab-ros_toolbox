#include "ros/ros.h"
#include "std_msgs/Int16MultiArray.h"
//#include "sensor_msgs/LaserScan.h"
#include <std_msgs/Float32.h>
#include <ros/subscriber.h>
#include <ros/publisher.h>
#include <ros/node_handle.h>
#include <cmath>

using namespace std;

//const res = 0.25;
//const thresh = th;

std_msgs::Float32 yawf;

void chatterCallback(const std_msgs::Int16MultiArray::ConstPtr& msg)
{ 
  int v = msg->data[0];
  int s = msg->data.size();
  if (v!= 1000 && s!=1)
  {
    int o1 =msg->data[0];
    int o2;
      int yaw1 ;
      int yaw2=0 ;
    for (int i=1;i<s;i++ )
    {
      o2= msg->data[i];
      yaw1 = o2 - o1;
      if(yaw1>=yaw2)
      {
        yawf.data= (o1 + o2)/2;
      }
      o1 = o2;
      yaw2=yaw1;
      
    }
  }
  if (v=1000) yawf.data= 1000;
  if (v!= 1000 && s==1) yawf.data= 90 + msg->data[0];

  
}
int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talkeroa");
  ros::NodeHandle n,n1;
  ros::Subscriber sub = n.subscribe("obstacle_detector", 1, chatterCallback);
  ros::Publisher yawf_pub = n1.advertise<std_msgs::Float32>("yaw_final", 1);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {

    yawf_pub.publish(yawf);

    ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}