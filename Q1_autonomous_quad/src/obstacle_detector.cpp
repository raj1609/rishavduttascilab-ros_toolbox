#include "ros/ros.h"
#include "std_msgs/Int16MultiArray.h"
#include "sensor_msgs/LaserScan.h"
#include <ros/subscriber.h>
#include <ros/publisher.h>
#include <ros/node_handle.h>
#include <cmath>

using namespace std;

float res = 0.25;
float th;
float thresh =th= 1.5;

std_msgs::Int16MultiArray nob_restheta;

void chatterCallback(const sensor_msgs::LaserScan& msg)
{
  int data[1083];
  nob_restheta.data.clear(); 
  data[0] = 0 ;
  data[1] =th-2;
  int j=0;
  int c[1081];
  c[0]= 1000;
  int d[1081];
  for(int i=2; i<1083; i++)
  {
    data[i] = msg.ranges[i-2];
     
      if (data[i]<=th)
     {
      if (data[i-1]>th)
      {
         data[0]++;
         j++;
      }
      if(c[j]>data[i]) 
        { 
          c[j]=data[i];
          d[j]=(i-2)*res-135;       
        }    
     }
  } 
  if (data[0]!=0)
  {
   for (int k=0;k<=j;k++) nob_restheta.data[k]=d[k] ;
  }
  if (data[0]=0) nob_restheta.data[0]=1000;
 }


int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talkerod");
  ros::NodeHandle n,n1;
  ros::Subscriber sub = n.subscribe("scan", 1, chatterCallback);
  ros::Publisher nob_restheta_pub = n1.advertise<std_msgs::Int16MultiArray >("obstacle_detector", 1);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {

    nob_restheta_pub.publish(nob_restheta);

    ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}