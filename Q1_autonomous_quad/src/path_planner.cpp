#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float32.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/NavSatStatus.h>
#include <std_msgs/Int64MultiArray.h>
#include <ros/subscriber.h>
#include <ros/publisher.h>
#include <ros/node_handle.h>
#include <cmath>

using namespace std;

//float roll;
double yaw_final, yaw_quad, dest_r, dest_theta;
double max_speed= 2.5;
double yaw_rate= 1.0;
double linear_vel = 1.0 ;
double yaw_scale = 120;
double linear_vel_scale= 2;
//std_msgs::Float64 gps1;
geometry_msgs::Twist final_twist;


void ImuCallback(const std_msgs::Float64& msg)
{
   yaw_quad= msg.data;
}


void ObstacleCallback(const std_msgs::Float32& msg)
{
   yaw_final= msg.data;
}
double delta_theta = yaw_final - yaw_quad ;
double yaw_cmd = (-1)*yaw_rate * delta_theta/yaw_scale ;

void DestCallback(const std_msgs::Int64MultiArray& msg)
{
   dest_theta = msg.data[0];
   dest_theta= dest_theta - yaw_quad;
   dest_r = msg.data[1];
}
double speed_x= linear_vel*dest_r/linear_vel_scale;



void pathplanner()
{
  if (yaw_final!= 1000)
  {
    final_twist.angular.z = yaw_cmd;
    final_twist.linear.x = 0.5;
    
  }
  if (yaw_final= 1000) 
  {
    final_twist.angular.z = (-1)*(dest_theta - yaw_quad)*yaw_rate/yaw_scale;
    if (speed_x>= max_speed) final_twist.linear.x= max_speed;
    else final_twist.linear.x= speed_x;

  }
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talkerp");
  ros::NodeHandle n,n1,n2,n3;
  ros::Subscriber sub = n.subscribe("theta_r_f", 1, DestCallback);
  ros::Subscriber sub1 = n1.subscribe("yaw_final", 1, ObstacleCallback);
  ros::Subscriber sub2 = n2.subscribe("yaw_quad", 1, ImuCallback);
  ros::Publisher final_twist_pub = n3.advertise<geometry_msgs::Twist>("final_twist", 1);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {
    pathplanner();
    final_twist_pub.publish(final_twist);

    ros::spinOnce();

    loop_rate.sleep();
   
  }

}