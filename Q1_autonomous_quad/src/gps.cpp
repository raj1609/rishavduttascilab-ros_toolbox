#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/Point.h>
#include <gps_common/conversions.h>
#include <sensor_msgs/NavSatStatus.h>
#include <ros/subscriber.h>
#include <ros/publisher.h>
#include <ros/node_handle.h>
#include <cmath>

using namespace std;

//float roll;

//std_msgs::Float64 gps1;
geometry_msgs::Point global;
void chatterCallback(const sensor_msgs::NavSatFix& msg)
{

  double X,Y;
  char zone;
  gps_common::LLtoUTM(msg.latitude,msg.longitude,Y,X, &zone);
  global.x = X;
  global.y = Y;

 //gps1.data = ;
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talker");
  ros::NodeHandle n, n1;
  ros::Subscriber sub = n.subscribe("fix", 1, chatterCallback);
  ros::Publisher global_pub = n1.advertise<geometry_msgs::Point>("global_xy", 1);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {

    global_pub.publish(global);

    ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}