# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a ROS package for making a quadrotor obstacle avoiding.

### How do I get set up? ###

THE PACKAGE Has been completely developed

1) It has 6 ros nodes
2) One of the nodes asks for final lat-long of destination of quad, as user input.
3) The package is compiled and ready to use.
4) I havent written the launch file yet, but it can be added later.
5) The package can be used by --
      rosrun autonomous_quad gps,
      rosrun autonomous_quad imu,
      rosrun autonomous_quad heading_director,
      rosrun autonomous_quad obstacle_detector,
      rosrun autonomous_quad obstacle_voider,
      rosrun autonomous_quad path_planner

### Contribution guidelines * Summary of set up
I have used standard gps_common/conversions.h for converting lat-long to UTM


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact